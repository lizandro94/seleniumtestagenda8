# Pruebas Unitarias- Agenda 8
***Generación de pruebas con Selenium***

El presente proyecto sigue las instrucciones estipuladas en la `Agenda 8` de la clase Ingeniería de Software. Se han desarrollado los 3 métodos de pruebas restantes (Resta, multiplicación y división) en el proyecto Web Calculator.

Los métodos de prueba se crearon en la clase `CalculatorFixture` El ChromeDriver se ejecutó en la dirección `C:\driver\`. Esta dirección se debe cambiar si se ejecuta el driver en otra ubicación. La Url donde se llevan a cabo las pruebas es `https://localhost:44348/Calculator`
**Integrantes**
 - David Bermúdez.
 - Leonardo Flores.
 - Leandro Fonseca.
 - Jasser Gutiérrez.